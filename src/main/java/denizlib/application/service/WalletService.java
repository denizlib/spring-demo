package denizlib.application.service;

import denizlib.application.service.dto.WalletDTO;
import denizlib.application.service.util.Exchange;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link denizlib.application.domain.Wallet}.
 */
public interface WalletService {

    /**
     * Save a wallet.
     *
     * @param walletDTO the entity to save.
     * @return the persisted entity.
     */
    WalletDTO save(WalletDTO walletDTO);

    /**
     * Get all the wallets.
     *
     * @return the list of entities.
     */
    List<WalletDTO> findAll();


    /**
     * Get the "id" wallet.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WalletDTO> findOne(Long id);

    /**
     * Delete the "id" wallet.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<WalletDTO> getCurrentUsersWallet(Long userId);

    void exchangeCurrency(Exchange exchange, Long userId);
}
