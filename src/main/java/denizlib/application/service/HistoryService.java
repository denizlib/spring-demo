package denizlib.application.service;

import denizlib.application.service.dto.HistoryDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link denizlib.application.domain.History}.
 */
public interface HistoryService {

    /**
     * Save a history.
     *
     * @param historyDTO the entity to save.
     * @return the persisted entity.
     */
    HistoryDTO save(HistoryDTO historyDTO);

    /**
     * Get all the histories.
     *
     * @return the list of entities.
     */
    List<HistoryDTO> findAll();


    /**
     * Get the "id" history.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HistoryDTO> findOne(Long id);

    /**
     * Delete the "id" history.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<HistoryDTO> findAllByUserId(Long userId);
}
