package denizlib.application.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link denizlib.application.domain.History} entity.
 */
public class HistoryDTO implements Serializable {

    private Long id;

    private String boughtCurrency;

    private String soldCurrency;

    private Double boughtAmount;

    private Double soldAmount;

    private Instant transactionDate;


    private Long userId;

    private String userLogin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBoughtCurrency() {
        return boughtCurrency;
    }

    public void setBoughtCurrency(String boughtCurrency) {
        this.boughtCurrency = boughtCurrency;
    }

    public String getSoldCurrency() {
        return soldCurrency;
    }

    public void setSoldCurrency(String soldCurrency) {
        this.soldCurrency = soldCurrency;
    }

    public Double getBoughtAmount() {
        return boughtAmount;
    }

    public void setBoughtAmount(Double boughtAmount) {
        this.boughtAmount = boughtAmount;
    }

    public Double getSoldAmount() {
        return soldAmount;
    }

    public void setSoldAmount(Double soldAmount) {
        this.soldAmount = soldAmount;
    }

    public Instant getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Instant transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HistoryDTO historyDTO = (HistoryDTO) o;
        if (historyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), historyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HistoryDTO{" +
            "id=" + getId() +
            ", boughtCurrency='" + getBoughtCurrency() + "'" +
            ", soldCurrency='" + getSoldCurrency() + "'" +
            ", boughtAmount=" + getBoughtAmount() +
            ", soldAmount=" + getSoldAmount() +
            ", transactionDate='" + getTransactionDate() + "'" +
            ", user=" + getUserId() +
            ", user='" + getUserLogin() + "'" +
            "}";
    }
}
