package denizlib.application.service.util;

public class Exchange {
    public String buyingCurrency;
    public String sellingCurrency;
    public Double buyingAmount;
}
