package denizlib.application.service.impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import denizlib.application.repository.UserRepository;
import denizlib.application.service.HistoryService;
import denizlib.application.service.WalletService;
import denizlib.application.domain.Wallet;
import denizlib.application.repository.WalletRepository;
import denizlib.application.service.dto.HistoryDTO;
import denizlib.application.service.dto.WalletDTO;
import denizlib.application.service.mapper.WalletMapper;
import denizlib.application.service.util.Exchange;
import denizlib.application.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Wallet}.
 */
@Service
@Transactional
public class WalletServiceImpl implements WalletService {

    private final Logger log = LoggerFactory.getLogger(WalletServiceImpl.class);

    private final WalletRepository walletRepository;

    private final WalletMapper walletMapper;

    private final UserRepository userRepository;

    private final HistoryService historyService;

    private final String uri = "https://api.exchangeratesapi.io";

    public WalletServiceImpl(WalletRepository walletRepository, WalletMapper walletMapper, UserRepository userRepository, HistoryService historyService) {
        this.walletRepository = walletRepository;
        this.walletMapper = walletMapper;
        this.userRepository = userRepository;
        this.historyService = historyService;
    }

    /**
     * Save a wallet.
     *
     * @param walletDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WalletDTO save(WalletDTO walletDTO) {
        log.debug("Request to save Wallet : {}", walletDTO);
        Wallet wallet = walletMapper.toEntity(walletDTO);
        wallet = walletRepository.save(wallet);
        return walletMapper.toDto(wallet);
    }

    /**
     * Get all the wallets.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<WalletDTO> findAll() {
        log.debug("Request to get all Wallets");
        return walletRepository.findAll().stream()
            .map(walletMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one wallet by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WalletDTO> findOne(Long id) {
        log.debug("Request to get Wallet : {}", id);
        return walletRepository.findById(id)
            .map(walletMapper::toDto);
    }

    /**
     * Delete the wallet by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Wallet : {}", id);
        walletRepository.deleteById(id);
    }

    @Override
    public List<WalletDTO> getCurrentUsersWallet(Long userId) {
        //User user = userRepository.findFirstById(userId);
        return walletMapper.toDto(walletRepository.findAllByUser(userId));
    }

    @Override
    public void exchangeCurrency(Exchange exchange, Long userId) {
        Wallet buyingWallet = walletRepository.findWalletByCurrency_CodeAndUser_Id(exchange.buyingCurrency, userId);
        Wallet sellingWallet = walletRepository.findWalletByCurrency_CodeAndUser_Id(exchange.sellingCurrency, userId);
        String path = "/latest?base=" + exchange.buyingCurrency + "&symbols=" + exchange.sellingCurrency;
        final HttpHeaders headers = new HttpHeaders();
        final HttpEntity<?> entity = new HttpEntity<>(headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(uri+path, HttpMethod.GET, entity, String.class);
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonTree = jsonParser.parse(response.getBody());
        if (jsonTree.isJsonObject())
        {
            JsonObject jsonObject = jsonTree.getAsJsonObject();
            Double exchangeRate = jsonObject.getAsJsonObject("rates").get(exchange.sellingCurrency).getAsDouble();
            if (sellingWallet.getAmount() - exchange.buyingAmount*exchangeRate >= 0)
            {
                sellingWallet.setAmount(sellingWallet.getAmount() - exchange.buyingAmount*exchangeRate);
                buyingWallet.setAmount(buyingWallet.getAmount() + exchange.buyingAmount);
                walletRepository.save(buyingWallet);
                walletRepository.save(sellingWallet);
                HistoryDTO history = new HistoryDTO();
                history.setBoughtAmount(exchange.buyingAmount);
                history.setBoughtCurrency(exchange.buyingCurrency);
                history.setSoldAmount(exchange.buyingAmount*exchangeRate);
                history.setSoldCurrency(exchange.sellingCurrency);
                history.setUserId(userId);
                history.setTransactionDate(Instant.now());
                historyService.save(history);
            }
            else
                throw new BadRequestAlertException("Not enough money", "Wallet", "not enough");
        }
    }
}
