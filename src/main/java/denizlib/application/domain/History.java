package denizlib.application.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A History.
 */
@Entity
@Table(name = "history")
public class History implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "bought_currency")
    private String boughtCurrency;

    @Column(name = "sold_currency")
    private String soldCurrency;

    @Column(name = "bought_amount")
    private Double boughtAmount;

    @Column(name = "sold_amount")
    private Double soldAmount;

    @CreatedDate
    @Column(name = "transaction_date", updatable = false)
    private Instant transactionDate;

    @ManyToOne
    @JsonIgnoreProperties("histories")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBoughtCurrency() {
        return boughtCurrency;
    }

    public History boughtCurrency(String boughtCurrency) {
        this.boughtCurrency = boughtCurrency;
        return this;
    }

    public void setBoughtCurrency(String boughtCurrency) {
        this.boughtCurrency = boughtCurrency;
    }

    public String getSoldCurrency() {
        return soldCurrency;
    }

    public History soldCurrency(String soldCurrency) {
        this.soldCurrency = soldCurrency;
        return this;
    }

    public void setSoldCurrency(String soldCurrency) {
        this.soldCurrency = soldCurrency;
    }

    public Double getBoughtAmount() {
        return boughtAmount;
    }

    public History boughtAmount(Double boughtAmount) {
        this.boughtAmount = boughtAmount;
        return this;
    }

    public void setBoughtAmount(Double boughtAmount) {
        this.boughtAmount = boughtAmount;
    }

    public Double getSoldAmount() {
        return soldAmount;
    }

    public History soldAmount(Double soldAmount) {
        this.soldAmount = soldAmount;
        return this;
    }

    public void setSoldAmount(Double soldAmount) {
        this.soldAmount = soldAmount;
    }

    public Instant getTransactionDate() {
        return transactionDate;
    }

    public History transactionDate(Instant transactionDate) {
        this.transactionDate = transactionDate;
        return this;
    }

    public void setTransactionDate(Instant transactionDate) {
        this.transactionDate = transactionDate;
    }

    public User getUser() {
        return user;
    }

    public History user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof History)) {
            return false;
        }
        return id != null && id.equals(((History) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "History{" +
            "id=" + getId() +
            ", boughtCurrency='" + getBoughtCurrency() + "'" +
            ", soldCurrency='" + getSoldCurrency() + "'" +
            ", boughtAmount=" + getBoughtAmount() +
            ", soldAmount=" + getSoldAmount() +
            ", transactionDate='" + getTransactionDate() + "'" +
            "}";
    }
}
