/**
 * View Models used by Spring MVC REST controllers.
 */
package denizlib.application.web.rest.vm;
