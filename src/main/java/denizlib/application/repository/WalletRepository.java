package denizlib.application.repository;

import denizlib.application.domain.User;
import denizlib.application.domain.Wallet;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Wallet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WalletRepository extends JpaRepository<Wallet, Long> {

    @Query("select wallet from Wallet wallet where wallet.user.login = ?#{principal.username}")
    List<Wallet> findByUserIsCurrentUser();

    @Query(
        value = "SELECT * FROM wallet WHERE user_id = ?1",
        nativeQuery = true)
    List<Wallet> findAllByUser(Long userId);

    Wallet findWalletByCurrency_CodeAndUser_Id(String currency, Long userId);
}
