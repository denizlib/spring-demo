package denizlib.application.repository;

import denizlib.application.domain.History;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the History entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HistoryRepository extends JpaRepository<History, Long> {

    @Query("select history from History history where history.user.login = ?#{principal.username}")
    List<History> findByUserIsCurrentUser();

    List<History> findAllByUser_Id(Long userId);

}
