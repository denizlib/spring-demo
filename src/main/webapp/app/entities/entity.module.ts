import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'wallet',
        loadChildren: './wallet/wallet.module#SpringDemoWalletModule'
      },
      {
        path: 'currency',
        loadChildren: './currency/currency.module#SpringDemoCurrencyModule'
      },
      {
        path: 'history',
        loadChildren: './history/history.module#SpringDemoHistoryModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SpringDemoEntityModule {}
