import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IHistory, History } from 'app/shared/model/history.model';
import { HistoryService } from './history.service';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-history-update',
  templateUrl: './history-update.component.html'
})
export class HistoryUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    boughtCurrency: [],
    soldCurrency: [],
    boughtAmount: [],
    soldAmount: [],
    transactionDate: [],
    userId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected historyService: HistoryService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ history }) => {
      this.updateForm(history);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(history: IHistory) {
    this.editForm.patchValue({
      id: history.id,
      boughtCurrency: history.boughtCurrency,
      soldCurrency: history.soldCurrency,
      boughtAmount: history.boughtAmount,
      soldAmount: history.soldAmount,
      transactionDate: history.transactionDate != null ? history.transactionDate.format(DATE_TIME_FORMAT) : null,
      userId: history.userId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const history = this.createFromForm();
    if (history.id !== undefined) {
      this.subscribeToSaveResponse(this.historyService.update(history));
    } else {
      this.subscribeToSaveResponse(this.historyService.create(history));
    }
  }

  private createFromForm(): IHistory {
    return {
      ...new History(),
      id: this.editForm.get(['id']).value,
      boughtCurrency: this.editForm.get(['boughtCurrency']).value,
      soldCurrency: this.editForm.get(['soldCurrency']).value,
      boughtAmount: this.editForm.get(['boughtAmount']).value,
      soldAmount: this.editForm.get(['soldAmount']).value,
      transactionDate:
        this.editForm.get(['transactionDate']).value != null
          ? moment(this.editForm.get(['transactionDate']).value, DATE_TIME_FORMAT)
          : undefined,
      userId: this.editForm.get(['userId']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHistory>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
