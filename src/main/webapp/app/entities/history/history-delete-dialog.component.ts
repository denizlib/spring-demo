import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHistory } from 'app/shared/model/history.model';
import { HistoryService } from './history.service';

@Component({
  selector: 'jhi-history-delete-dialog',
  templateUrl: './history-delete-dialog.component.html'
})
export class HistoryDeleteDialogComponent {
  history: IHistory;

  constructor(protected historyService: HistoryService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.historyService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'historyListModification',
        content: 'Deleted an history'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-history-delete-popup',
  template: ''
})
export class HistoryDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ history }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HistoryDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.history = history;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/history', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/history', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
