import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IHistory } from 'app/shared/model/history.model';
import { AccountService } from 'app/core';
import { HistoryService } from './history.service';

@Component({
  selector: 'jhi-history',
  templateUrl: './history.component.html'
})
export class HistoryComponent implements OnInit, OnDestroy {
  histories: IHistory[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected historyService: HistoryService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.historyService
      .query()
      .pipe(
        filter((res: HttpResponse<IHistory[]>) => res.ok),
        map((res: HttpResponse<IHistory[]>) => res.body)
      )
      .subscribe(
        (res: IHistory[]) => {
          this.histories = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInHistories();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IHistory) {
    return item.id;
  }

  registerChangeInHistories() {
    this.eventSubscriber = this.eventManager.subscribe('historyListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
