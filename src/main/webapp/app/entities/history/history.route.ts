import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { History } from 'app/shared/model/history.model';
import { HistoryService } from './history.service';
import { HistoryComponent } from './history.component';
import { HistoryDetailComponent } from './history-detail.component';
import { HistoryUpdateComponent } from './history-update.component';
import { HistoryDeletePopupComponent } from './history-delete-dialog.component';
import { IHistory } from 'app/shared/model/history.model';

@Injectable({ providedIn: 'root' })
export class HistoryResolve implements Resolve<IHistory> {
  constructor(private service: HistoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHistory> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<History>) => response.ok),
        map((history: HttpResponse<History>) => history.body)
      );
    }
    return of(new History());
  }
}

export const historyRoute: Routes = [
  {
    path: '',
    component: HistoryComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Histories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HistoryDetailComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Histories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HistoryUpdateComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Histories'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HistoryUpdateComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Histories'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const historyPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HistoryDeletePopupComponent,
    resolve: {
      history: HistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'Histories'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
