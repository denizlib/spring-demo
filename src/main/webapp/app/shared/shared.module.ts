import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SpringDemoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [SpringDemoSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [SpringDemoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SpringDemoSharedModule {
  static forRoot() {
    return {
      ngModule: SpringDemoSharedModule
    };
  }
}
