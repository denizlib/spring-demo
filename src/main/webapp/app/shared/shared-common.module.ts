import { NgModule } from '@angular/core';

import { SpringDemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [SpringDemoSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [SpringDemoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class SpringDemoSharedCommonModule {}
