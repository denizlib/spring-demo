export interface IWallet {
  id?: number;
  amount?: number;
  userLogin?: string;
  userId?: number;
  currencyCode?: string;
  currencyId?: number;
}

export class Wallet implements IWallet {
  constructor(
    public id?: number,
    public amount?: number,
    public userLogin?: string,
    public userId?: number,
    public currencyCode?: string,
    public currencyId?: number
  ) {}
}
