export interface ICurrency {
  id?: number;
  code?: string;
}

export class Currency implements ICurrency {
  constructor(public id?: number, public code?: string) {}
}
