import { Moment } from 'moment';

export interface IHistory {
  id?: number;
  boughtCurrency?: string;
  soldCurrency?: string;
  boughtAmount?: number;
  soldAmount?: number;
  transactionDate?: Moment;
  userLogin?: string;
  userId?: number;
}

export class History implements IHistory {
  constructor(
    public id?: number,
    public boughtCurrency?: string,
    public soldCurrency?: string,
    public boughtAmount?: number,
    public soldAmount?: number,
    public transactionDate?: Moment,
    public userLogin?: string,
    public userId?: number
  ) {}
}
